#
#  Be sure to run `pod spec lint ModuleLogIn.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|
  s.name         = "ModuleLogIn"
  s.version      = "0.0.1"
  s.summary      = "A meningful description for this pod, change later"
  s.description  = "A short description of ModuleLogIn. A short description of ModuleLogIn."
  s.homepage     = "https://bitbucket.org/moduluxstudio/ios-login.git"
  s.license      = "MIT"
  s.author             = { "Jonathan Silva" => "jsilva@syesoftware.com" }
  # s.platform     = :ios
  # s.platform     = :ios, "5.0"

  s.source       = { :git => "https://bitbucket.org/moduluxstudio/ios-login.git", :tag => "#{s.version}" }
  s.source_files  = "module-log-in", "module-log-in/**/*.{h,m,swift}"
  #s.dependency "ModuleMobileApi", :git => "/Users/jonathansilva/Documents/Repos/modulux/ios-mobile-api"

end
